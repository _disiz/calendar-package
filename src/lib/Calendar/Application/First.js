import React, { Component, Fragment } from 'react'
import App from './App';
import '../Component/Calendar.css'
import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux'
import { apiCall } from '../actions/apiCall';
import { loggedCalendarUser } from '../actions/calendar';

class First extends Component {
  componentWillMount(){
    var user = sessionStorage.getItem('calendar_user')
    this.props.loggedCalendarUser(user)
    this.props.apiCall()
  }

  render() {
    return (
        <App/>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => bindActionCreators({
  apiCall: apiCall,
  loggedCalendarUser: loggedCalendarUser
},dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(First)

