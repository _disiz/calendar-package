import React, { Component } from 'react';
import Modal from '../Modal/Modal';
import { CalendarContext } from '../State/Globalstate';
import Calendar from '../Component/Calendar';
import Header from '../Component/Header';
import Sidebar from '../Component/Sidebar';
import Globalstate from '../State/Globalstate';


class Application extends Component {
  static contextType = CalendarContext
  render() {
    return (
      <div className='App-header'>
        <div style={{ opacity: this.context.isOpen ? '0.4' : '1' }}
          onClick={this.context.isOpen ?
            this.context.handleClick : () => { }}>
          <div className='calendar'>
            <Header />
          </div>
          {this.context.forSidebar ?
            <div style={{ float: 'left', width: '250px' }} >
              <Sidebar history={this.props.history} />
            </div> : ''}
          <div className={`${this.context.forSidebar ?
            'withSidebar' : ''}`}>
            <div onClick={this.context.isOpen ?
              this.context.handleClick : () => { }} >
              <Calendar /></div>
          </div>
        </div>
        <div style={{ clear: 'both' }} >
          {this.context.isOpen ?
            <Modal
              isOpen={this.context.isOpen}
              handleClick={() => this.context.handleClick()} /> : ''
          }
        </div>
      </div>
    )
  }
}


export default class App extends Component {
  render() {
    return (
      <Globalstate>
        <Application history={ this.props.history} />
      </Globalstate>
    )
  }
}


