import React, { Component } from 'react'
import { CalendarContext } from '../State/Globalstate';

export default class extends Component {
    static contextType = CalendarContext
    render() {
    return (
      <div> 
        { this.context.eventList.map((i,index)=> 
            < span key={index} onClick={() => this.context.seeDetail(i.detail)}>{i.event}</span>)}
      </div>
    )
  }
}
