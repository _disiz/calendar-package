export const CALENDAR_PENDING = 'CALENDAR_PENDING'
export const FETCH_CALENDAR = 'FETCH_CALENDAR'
export const FETCH_CALENDAR_ERROR = 'FETCH_CALENDAR_ERROR'
export const EDIT_CALENDAR_EVENT = 'EDIT_CALENDAR_EVENT'
export const LOGGED_CALENDAR_USER = 'LOGGED_CALENDAR_USER'
export const CALENDAR_USER_LOGIN = 'CALENDAR_USER_LOGIN'
export const CALENDAR_CURRENT_MONTH = 'CALENDAR_CURRENT_MONTH'
export const CALENDAR_CURRENT_YEAR = 'CALENDAR_CURRENT_YEAR'
export const SHOW_CALENDAR = 'SHOW_CALENDAR'
export const CHANGE_CALENDAR = 'CHANGE_CALENDAR'
export const SHARE_CALENDAR = 'SHARE_CALENDAR'
export const CALENDAR_USER_GROUP = 'CALENDAR_USER_GROUP'
export const CALENDAR_SHARE_PERMISSION = 'CALENDAR_SHARE_PERMISSION'

const fetchCalendarPending = () => ({
        type: CALENDAR_PENDING
})

const fetchCalendar = calendar => ({
        type: FETCH_CALENDAR,
        calendar: calendar
})

const fetchCalendarError = error => ({
        type: FETCH_CALENDAR_ERROR,
        error: error
})

const editCalendarEvent = editing => ({
        type: EDIT_CALENDAR_EVENT,
        editing: editing
})

const userLoginCalendar = (id) => ({
        type: CALENDAR_USER_LOGIN,
        user_calendar: id,
})

const loggedCalendarUser = (id) => ({
        type: LOGGED_CALENDAR_USER,
        user: id
})

const setCurrentMonth = (id) => ({
        type: CALENDAR_CURRENT_MONTH,
        currentMonth: id,
})
const setYear = (yr) => ({
        type: CALENDAR_CURRENT_YEAR,
        currentYear: yr
})

const changeCalendar = (i,cal) => ({
        type: CHANGE_CALENDAR,
        currentMonth: i,
        calendar: cal
})

const showCalendar = (id) => ({
        type: SHOW_CALENDAR,
        calendar_id: id
})

const shareCalendar = (i) => ({
        type: SHARE_CALENDAR,
        cal_share: i,
})

const getCalendarUserGroup = (i) => ({
        type: CALENDAR_USER_GROUP,
        user_group: i,
})

const calendarPermission = (i) => ({
        type: CALENDAR_SHARE_PERMISSION,
        permission: i,
})

export {
    fetchCalendarPending,
    fetchCalendar,
    fetchCalendarError,
    editCalendarEvent,
    userLoginCalendar,
    loggedCalendarUser,
    setCurrentMonth,
    setYear,
    showCalendar,
    changeCalendar,
    shareCalendar,
    getCalendarUserGroup,
    calendarPermission
}