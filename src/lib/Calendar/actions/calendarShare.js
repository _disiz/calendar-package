import axios from 'axios'
import { shareCalendar, getCalendarUserGroup, calendarPermission } from './calendar';

let baseUrl = 'https://backend-calendar.dev.genisys.cf'

export function calSharing(id) {
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    return dispatch=> {
        dispatch(shareCalendar(id))
        axios.get(baseUrl + '/groups', { params: {email: user},headers: { Authorization: 'Bearer ' + token } })
        .then(out => {
            console.log('group', out)
            dispatch(getCalendarUserGroup(out.data.data))
            axios.get(baseUrl + '/event/permissions', {params: {email: user}, headers: { Authorization: 'Bearer ' + token } })
                .then(out => {
                    console.log('permission', out)
                    dispatch(calendarPermission(out.data.data))
                })
        })
        .catch(error => {
            console.error(error)
        })
    }
}
