import React, { Component } from 'react'
import AddEvent from './AddEvent';
import {connect} from 'react-redux'
import { CalendarContext } from '../State/Globalstate';
import Events from './Events';
import { eventDelete } from '../actions/apiCall';
import { bindActionCreators } from 'redux';
import EventList from './EventList';
import Share from '../Component/Share'


class Modal extends Component {
    static contextType = CalendarContext
    constructor(props){
        super(props)
        this.state = {
        ask: false,
        delete: '',
    }
}

    confirmation = () => {
        return <div>
            Continue deleting <b><i>{this.context.clickedEvent.title}</i></b> ?<br/><br/>
            <span style={{float: 'right'}} >
                <button className='btn btn-secondary' 
                onClick={() => this.setState({ask: false})} >Cancel</button>
                <button className='btn btn-danger'
                onClick={()=> this.deleteEvent()} >Delete</button>
            </span>
        </div>
    }
    deleteEvent = () => {
        const {clickedEvent, currentYear, currentMonth} = this.context
        const selectedEvent = {
            id: clickedEvent.id,
            start_date: clickedEvent.start_date,
            end_date: clickedEvent.end_date,
            is_recurring: clickedEvent.is_recurring,
            origid: clickedEvent.origid,
            redit: this.state.delete
        }
        this.props.eventDelete(selectedEvent,currentMonth,currentYear)
        this.context.handleClick()
    }
    render() {
        const { seeEvent, handleClick, clickedEvent, showList, forMore, forSharing } = this.context
        const deleteStyle={
            fontSize: '120%',
            marginTop: '5px',
            marginLeft: '5px',
        }
        return (
            <div className='modal-wrapper' style={{width: showList? '260px': seeEvent ? '400px':'auto', height: forMore ? '400px':'',overflowX: 'hidden', borderBottom: seeEvent ? `2px solid ${clickedEvent.color.color}`: null}} >
                <div className="modal-body" >
                    {this.state.ask ? this.confirmation()
                    :<><small style={{float: 'right'}} >
                    {seeEvent ? <>{clickedEvent.event_permission.includes('modify') || clickedEvent.event_permission.includes('all') ? <button className='icon' 
                        onClick={() => this.context.editCalendarEvent()} >edit</button> : null}
                        {clickedEvent.is_recurring === 1 ? 
                        <select style={deleteStyle}
                            onChange={(e) => this.setState({
                                ask: true,
                                delete: e.target.value
                            })} >
                            <option>Delete</option>                            
                            <option value='single'>This Event</option>
                            <option value='future'>This and Following Events</option>
                            <option value='all'>All Events</option>
                        </select>
                        : <button className='icon' 
                        onClick={() => this.setState({ask: true})} >delete</button>}
                        </>
                        : ''}
                    <button className='icon' style={{paddingRight: showList ? '0px': ''}} onClick={() => handleClick()} >close</button>
                    </small>{seeEvent ? <br/>: ''}
                    {seeEvent ? <Events /> : showList ? <EventList/> : forSharing ? <Share/> :<AddEvent/>}</>}
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    eventDelete: eventDelete,
}, dispatch)

export default connect(
    null,
    mapDispatchToProps
)(Modal)