import {fetchCalendarPending, fetchCalendar, fetchCalendarError, userLoginCalendar, setCurrentMonth, changeCalendar, setYear } from './calendar'
import axios from 'axios'

let baseUrl = 'https://backend-calendar.dev.genisys.cf'

export function apiCall(){
    let token = sessionStorage.getItem('calendar_access')    
    let user = sessionStorage.getItem('calendar_user')
    return dispatch => {
        dispatch(fetchCalendarPending())
        axios.get(baseUrl+'/calendars',{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
            .then(out => {
                console.log(out)
                let cal = out.data.data['events']
                let year = Object.keys(cal)
                dispatch(userLoginCalendar(out.data.data['calendars']))
                if (year.length === 1){
                    dispatch(fetchCalendar(cal[year[0]]))
                    let months = Object.keys(cal[year[0]])
                    dispatch(setCurrentMonth(parseInt(months[0])))
                    dispatch(setYear(parseInt(year[0])))
                }else{
                    let thisYear = Object.keys(cal[year[0]]).length>1 ? year[0] : year[1]
                    let calendar = Object.assign(cal[year[0]],cal[year[1]])
                    dispatch(fetchCalendar(calendar))
                    let months = Object.keys(calendar)
                    dispatch(setCurrentMonth(parseInt(months[0])))
                    dispatch(setYear(parseInt(thisYear)))
                }
            })
            .catch(error => {
                dispatch(fetchCalendarError(error))
            })
    }
}

export function eventCreate(d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')  
    return dispatch => {
        axios.post(baseUrl+'/events/',d,{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email'] = user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar))
            })
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function eventDelete(d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    d['email'] = user
    return dispatch => {
        axios.delete(baseUrl+'/events/'+d.id,{params: d,headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email'] = user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar))
            })        
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function eventUpdate(d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')   
    return dispatch => {
        axios.put(baseUrl+'/events/'+d.id,d,{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email'] = user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})            
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar))
            })
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function addCalendar(d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    return dispatch => {
        axios.post(baseUrl+'/calendars/',d,{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email'] = user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})            
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar))                
                dispatch(userLoginCalendar(out.data.data['calendars']))
            })
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function deleteCalendar(d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    return dispatch => {
        axios.delete(baseUrl+'/calendars/'+d,{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email']=user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})            
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar)) 
                dispatch(userLoginCalendar(out.data.data['calendars']))
            })
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function editCalendar(id,d,m,y){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    return dispatch => {
        axios.put(baseUrl+'/calendars/'+id,d,{params: {email: user},headers: {Authorization: 'Bearer '+ token}})
        .then(res => {
            console.log(res)
            let date = dateInterval(m,y)
            date['email']=user
            axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})            
            .then(out => {
                let calendar = fetchingCalendar(out)
                dispatch(fetchCalendar(calendar)) 
                dispatch(userLoginCalendar(out.data.data['calendars']))
            })
        })
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })
    }
}

export function getNewCal(date,m){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')   
    date['email'] = user 
    return dispatch => {
        axios.get(baseUrl+'/calendars',{params: date,headers: {Authorization: 'Bearer '+ token}})
        .then(out => {
            let cal = out.data.data['events']
            let year = Object.keys(cal)
            if (year.length===1){
                dispatch(changeCalendar(parseInt(m),cal[year[0]]))
                dispatch(setYear(parseInt(year[0])))
            }else{
                let thisYear = Object.keys(cal[year[0]]).length>1 ? year[0] : year[1]
                let calendar = {...cal[year[0]],...cal[year[1]]}
                dispatch(changeCalendar(parseInt(m),calendar))
                dispatch(setYear(parseInt(thisYear)))
            }

        }) 
        .catch(error => {
            dispatch(fetchCalendarError(error))
        })      
    }
}

function dateInterval(m,y){
    let monthStart = m < 1 ? 12-m :m
    monthStart = monthStart < 10 ? '0'+monthStart : monthStart
    let yearStart = m < 1 ? y-1 : y
    let monthEnd = m > 9 ? m+3-12 :m+3
    monthEnd = monthEnd < 10 ? '0'+monthEnd : monthEnd
    let yearEnd = m > 9 ? y+1 : y
    let date = {
      start_date: yearStart+'-'+monthStart+'-01',
      end_date: yearEnd+'-'+monthEnd+'-01'
    }
    return date
}

function fetchingCalendar(out){
    let cal = out.data.data['events']
    let year = Object.keys(cal)
    if (year.length===1){
        return cal[year[0]]
    }else{
        let calendar = {...cal[year[0]],...cal[year[1]]}
        return calendar
    }
}