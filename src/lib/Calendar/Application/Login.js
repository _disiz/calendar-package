import React, {Component} from 'react'
import axios from 'axios'
import store from '../store'
import {Provider} from 'react-redux'
import First from './First'

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state ={
            loading: true
        }
    }
    
    componentDidMount(){
        let baseUrl = 'https://backend-calendar.dev.genisys.cf'
        sessionStorage.setItem('calendar_user',this.props.email)
        if(!sessionStorage.getItem('calendar_access')) {
            const data= {
            grant_type: 'password',
            username: this.props.email,
            password: this.props.password,
            }
            axios.post(baseUrl+'/users/login',data)
            .then(out=> {
                sessionStorage.setItem('calendar_access', out.data.access_token )
                this.setState({ loading: false })
            })
            .catch(error => {
                console.log(error)
            })  
        }

    }
    
    render() {
        return (<Provider store={store}>
            <First/>
            </Provider>
        )
    }
}
