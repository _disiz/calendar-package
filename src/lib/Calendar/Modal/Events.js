import React, { Component } from 'react'
import { CalendarContext } from '../State/Globalstate';

export default class Events extends Component {
    static contextType = CalendarContext
    render() {
        const { title, start_date, end_date, description, visibility, formatted_rrule, is_full_day_event } = this.context.clickedEvent
        console.log(this.context.clickedEvent)
        if (this.context.clickedEvent !== undefined) {
            return <div>
                <h3>{title}</h3>
                <div className='icon'
                    style={{ padding: '5px' }} >access_time</div>
                {is_full_day_event === 1 ? start_date.split(' ')[0] : start_date} - {is_full_day_event === 1 ? end_date.split(' ')[0] : end_date}<br />
                {formatted_rrule.length !== 0 ? <><div className='icon' style={{ padding: '5px' }} >repeat</div>
                    {formatted_rrule.human_readable}<br /></> : <></>}
                <div className='icon' style={{ padding: '5px' }} >card_travel</div>
                {visibility}<br />
                <p dangerouslySetInnerHTML={{ __html: description }} />
            </div>
        }
        else return null
    }
}